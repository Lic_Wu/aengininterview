//
//  NSString+ReverseString.m
//  AenginInterviewTest
//
//  Created by Lic on 2017/9/11.
//  Copyright © 2017年 LicStudio. All rights reserved.
//

#import "NSString+ReverseString.h"

@implementation NSString (ReverseString)

- (NSString *)reversingStringM1 {
    
    NSUInteger len = [self length];
    
    if (len < 2) {
        return self;
    }
    
    NSMutableString *reverseString = [[NSMutableString alloc] initWithCapacity:len];
    for(int i=len-1; i>=0; i--) {
        [reverseString appendFormat:@"%c",[self characterAtIndex:i]];
    }
    
    return [reverseString copy];
}

- (NSString *)reversingStringM2 {
    
    NSUInteger len = [self length];
    unichar *buffer = malloc(len * sizeof(unichar));
    
    if (buffer == nil)
        return nil;
    
    [self getCharacters:buffer];
    
    for (NSUInteger stPos=0, endPos=len-1; stPos < len/2; stPos++, endPos--) {
        unichar temp = buffer[stPos];
        buffer[stPos] = buffer[endPos];
        buffer[endPos] = temp;
    }
    
    return [[NSString alloc] initWithCharactersNoCopy:buffer length:len freeWhenDone:YES];
}

- (NSString *)reversingStringM3 {
    
    if ([self length] < 2) {
        return self;
    } else {
        return [[[self substringFromIndex:1] reversingStringM3] stringByAppendingString:[self substringToIndex:1]];
    }
}

@end
