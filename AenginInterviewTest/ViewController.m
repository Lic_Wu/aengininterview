//
//  ViewController.m
//  AenginInterviewTest
//
//  Created by Lic on 2017/9/11.
//  Copyright © 2017年 LicStudio. All rights reserved.
//

#import "ViewController.h"
#import "NSString+ReverseString.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString *myString = @"abcde";
    
    NSLog(@"reverseString1: %@", [myString reversingStringM1]);
    NSLog(@"reverseString2: %@", [myString reversingStringM2]);
    NSLog(@"reverseString3: %@", [myString reversingStringM3]);

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

#pragma mark -

@end
